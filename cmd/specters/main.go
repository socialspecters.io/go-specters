package main

import (
	"context"
	"fmt"
	logging "github.com/ipfs/go-log/v2"
	"github.com/namsral/flag"
	"gitlab.com/socialspecters.io/go-specters/pkg/specters"
	"gitlab.com/socialspecters.io/go-specters/pkg/util"
	"os"
	"os/signal"
	"runtime"
)

// Version is the version of docdb-console
var Version = "No Version Provided"

var log = logging.Logger("specters")

var (
	debug       bool
	showVersion bool
)

func main() {
	fs := flag.NewFlagSetWithEnvPrefix(os.Args[0], "SPCTRS", 0)

	repo := fs.String("repo", ".specters", "Repo location")
	fs.BoolVar(&debug, "debug", false, "Enables debug logging")
	fs.BoolVar(&debug, "d", false, "Enables debug logging")
	fs.BoolVar(&showVersion, "version", false, "Print version information.")
	fs.BoolVar(&showVersion, "v", false, "Print version information.")
	if err := fs.Parse(os.Args[1:]); err != nil {
		log.Fatal(err)
	}

	if showVersion {
		fmt.Printf(`specters %s, Compiler: %s %s, Copyright (C) 2022 Social Specters.`,
			Version,
			runtime.Compiler,
			runtime.Version())
		fmt.Println()
		os.Exit(0)
	}

	var err error

	err = logging.SetLogLevel("specters", util.LevelFromDebugFlag(debug))
	if err != nil {
		log.Fatal(err)
	}

	log.Debugf("debug: %v", debug)
	log.Debugf("repo: %v", *repo)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	s, err := specters.Init(ctx, specters.Config{
		Debug: debug,
		Repo:  *repo,
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Info("==> Social Specters configuration:")
	log.Info()
	log.Infof("         Go Version: %v", runtime.Version())
	log.Infof("            Version: %v", Version)
	log.Infof("          Log Level: %v", util.LevelFromDebugFlag(debug))
	log.Infof("               Repo: %v", *repo)
	log.Infof("       IPFS peer ID: %v", s.IPFSNode.ID())
	log.Infof("        Private Key: %v", s.IPFSNode.ConfigEncodePrivateKey())
	log.Infof("    Specter Address: %v", s.Specter.PublicProfileAddress())
	log.Info()
	log.Info("==> Welcome to Social Specters! Log data will stream in below:")

	handleInterrupt(func() {
		s.Close()
	})
}

func handleInterrupt(stop func()) {
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt) //nolint:all
	<-quit
	log.Info("Gracefully stopping... (press Ctrl+C again to force)")
	stop()
	os.Exit(1)
}
