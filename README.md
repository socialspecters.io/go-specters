# Go Specters

[![Pipeline Status](https://gitlab.com/socialspecters.io/go-specters/badges/main/pipeline.svg)](https://gitlab.com/socialspecters.io/go-specters/-/commits/main)
[![Coverage Report](https://gitlab.com/socialspecters.io/go-specters/badges/main/coverage.svg)](https://gitlab.com/socialspecters.io/go-specters/-/commits/main)
[![Go Report](https://img.shields.io/badge/go%20report-A+-brightgreen.svg?style=flat)](https://goreportcard.com/report/gitlab.com/socialspecters.io/go-specters)
[![License GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](./LICENSE)
