GO_BINARY ?= "go"
VERSION := $(shell git log --pretty="%H" -n1 HEAD)
PKG := "."
PKG_LIST := $(shell ${GO_BINARY} list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

# Get the dependencies
dep:
	@${GO_BINARY} get -v -d ./...
.PHONY: dep

# Build binaries
build: dep
	@echo "Compiling ${VERSION} version"
	@${GO_BINARY} build -ldflags '-s -w -X main.Version=${VERSION}' -o bin/specters cmd/specters/main.go
.PHONY: build

# Clean build output
clean:
	@rm -rf bin/* *.out */*.out
.PHONY: clean

# Lint the files
lint:
	@golangci-lint run --timeout=5m --print-issued-lines=false --out-format code-climate:gl-code-quality-report.json,line-number
.PHONY: lint

# Run unit tests
unit:
	@${GO_BINARY} test -v -covermode=count -coverprofile=coverage.out ${PKG_LIST}
.PHONY: unit

# Run data race detector
race: dep
	@${GO_BINARY} test -race -short ${PKG_LIST}
.PHONY: race
