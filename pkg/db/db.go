package db

import (
	logging "github.com/ipfs/go-log/v2"
)

var (
	loggingSetLogLevel = logging.SetLogLevel
	log                = logging.Logger("specters:db")
)

// DB is an interface for specter storage
type DB interface {
	// ID returns the DB ID
	ID() string

	// PublicProfile gets the Specter public profile
	PublicProfile() (PublicProfile, error)

	// Close closes the connections of DB
	Close() error
}

// PublicProfile is an interface for the Specter public profile
type PublicProfile interface {
	// Address Returns the address for the current store
	Address() string

	// All Returns a consolidated key value map from the entries of this store
	All() map[string][]byte

	// Put Sets the value for a key of the map
	Put(key string, value []byte) error

	// Delete Clears the value for a key of the map
	Delete(key string) error

	// Get Retrieves the value for a key of the map
	Get(key string) ([]byte, error)

	// Close closes the connections of DB
	Close() error
}
