package db

import (
	orbitdb "berty.tech/go-orbit-db"
	"context"
	"gitlab.com/socialspecters.io/go-specters/pkg/ipfs"
	"gitlab.com/socialspecters.io/go-specters/pkg/util"
	"go.uber.org/zap"
)

// OrbitDB is an instance of a OrbitDB database
type OrbitDB struct {
	logger  *zap.Logger
	orbitDB orbitdb.OrbitDB
	ctx     context.Context
}

// NewOrbitDB creates a new instance of OrbitDB
func NewOrbitDB(ctx context.Context, ipfsNode ipfs.Node, config Config) (DB, error) {
	var err error

	err = loggingSetLogLevel("specters:db", util.LevelFromDebugFlag(config.Debug))
	if err != nil {
		return nil, err
	}

	log.Info("creating OrbitDB")

	logger, _ := zap.NewProduction()
	if config.Debug {
		logger, _ = zap.NewDevelopment()
	}

	orbitDB, err := orbitdb.NewOrbitDB(ctx, ipfsNode.CoreAPI(), &orbitdb.NewOrbitDBOptions{
		Directory: &config.CachePath,
		Logger:    logger,
	})
	if err != nil {
		return nil, err
	}

	log.Info("finished creating OrbitDB")

	return &OrbitDB{
		logger:  logger,
		orbitDB: orbitDB,
		ctx:     ctx,
	}, nil
}

// ID returns the DB ID
func (db *OrbitDB) ID() string {
	return db.orbitDB.Identity().ID
}

// Close closes the connections of OrbitDB
func (db *OrbitDB) Close() error {
	return db.orbitDB.Close()
}
