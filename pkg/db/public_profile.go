package db

import (
	orbitdb "berty.tech/go-orbit-db"
	"context"
)

// PublicProfile gets the Specter PublicProfileOrbitDB
func (db *OrbitDB) PublicProfile() (PublicProfile, error) {
	log.Debug("opening PublicProfile OrbitDB")

	var err error

	replicate := false
	create := true
	storeType := "keyvalue"

	addr, err := db.orbitDB.DetermineAddress(db.ctx, "specter/profile", storeType, &orbitdb.DetermineAddressOptions{
		Replicate: &replicate,
	})
	if err != nil {
		return nil, err
	}

	publicProfile, err := db.orbitDB.Open(db.ctx, addr.String(), &orbitdb.CreateDBOptions{
		Create:    &create,
		StoreType: &storeType,
		Logger:    db.logger,
	})
	if err != nil {
		return nil, err
	}

	err = publicProfile.Load(db.ctx, -1)
	if err != nil {
		return nil, err
	}

	log.Debug("finished opening PublicProfile OrbitDB")

	return &PublicProfileOrbitDB{
		store: publicProfile.(orbitdb.KeyValueStore),
		ctx:   db.ctx,
	}, nil
}

// PublicProfileOrbitDB is an instance of a PublicProfile database
type PublicProfileOrbitDB struct {
	store orbitdb.KeyValueStore
	ctx   context.Context
}

// Address Returns the address for the current store
func (db *PublicProfileOrbitDB) Address() string {
	return db.store.Address().String()
}

// All Returns a consolidated key value map from the entries of this store
func (db *PublicProfileOrbitDB) All() map[string][]byte {
	return db.store.All()
}

// Put Sets the value for a key of the map
func (db *PublicProfileOrbitDB) Put(key string, value []byte) error {
	_, err := db.store.Put(db.ctx, key, value)
	return err
}

// Delete Clears the value for a key of the map
func (db *PublicProfileOrbitDB) Delete(key string) error {
	_, err := db.store.Delete(db.ctx, key)
	return err
}

// Get Retrieves the value for a key of the map
func (db *PublicProfileOrbitDB) Get(key string) ([]byte, error) {
	return db.store.Get(db.ctx, key)
}

// Close closes the connections of PublicProfileOrbitDB
func (db *PublicProfileOrbitDB) Close() error {
	return db.store.Close()
}
