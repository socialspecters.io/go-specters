package ipfs

import (
	"context"
	"fmt"
	"github.com/ipfs/boxo/ipld/unixfs"
	"github.com/ipfs/boxo/path"
	"github.com/ipfs/kubo/config"
	"github.com/ipfs/kubo/core"
	"github.com/ipfs/kubo/core/coreapi"
	icore "github.com/ipfs/kubo/core/coreiface"
	"github.com/ipfs/kubo/core/coreiface/options"
	"github.com/ipfs/kubo/core/node/libp2p"
	"github.com/ipfs/kubo/plugin/loader"
	"github.com/ipfs/kubo/repo/fsrepo"
	"github.com/libp2p/go-libp2p/core/crypto"
	"github.com/libp2p/go-libp2p/core/peer"
	"gitlab.com/socialspecters.io/go-specters/pkg/util"
	"os"
	"path/filepath"
	"sync"
)

// KuboNode is an instance of a IPFS Kubo node
type KuboNode struct {
	ipfsNode *core.IpfsNode
	coreAPI  icore.CoreAPI
	ctx      context.Context
}

// NewKuboNode creates a new instance of IPFS Kubo Node
func NewKuboNode(ctx context.Context, cfg Config) (Node, error) {
	err := loggingSetLogLevel("specters:ipfs", util.LevelFromDebugFlag(cfg.Debug))
	if err != nil {
		return nil, err
	}

	log.Info("creating IPFS node")

	if err := setupPlugins(cfg.RepoPath); err != nil {
		return nil, err
	}

	err = doInit(cfg.RepoPath)
	if err != nil {
		return nil, err
	}

	node, err := createNode(ctx, cfg.RepoPath)
	if err != nil {
		return nil, err
	}

	api, err := coreapi.NewCoreAPI(node)
	if err != nil {
		return nil, err
	}

	log.Info("finished creating IPFS node")

	return &KuboNode{
		ipfsNode: node,
		coreAPI:  api,
		ctx:      ctx,
	}, nil
}

// Connect connects the Node to the IPFS peers
func (node *KuboNode) Connect(peerInfos []peer.AddrInfo) error {
	log.Info("connecting IPFS peers")

	var wg sync.WaitGroup

	wg.Add(len(peerInfos))
	for idx := range peerInfos {
		go func(peerInfo *peer.AddrInfo) {
			defer wg.Done()
			err := node.coreAPI.Swarm().Connect(node.ctx, *peerInfo)
			if err != nil {
				log.Infof("failed to connect to %s: %s", peerInfo.ID, err)
			}
		}(&peerInfos[idx])
	}
	wg.Wait()

	log.Info("finished connecting IPFS peers")

	return nil
}

// ID returns the node peer ID
func (node *KuboNode) ID() string {
	return node.ipfsNode.Identity.String()
}

// ConfigEncodePrivateKey returns the node private key encoded
func (node *KuboNode) ConfigEncodePrivateKey() string {
	raw, _ := node.ipfsNode.PrivateKey.Raw()
	return crypto.ConfigEncodeKey(raw)
}

// ConfigEncodePublicKey returns the node public key encoded
func (node *KuboNode) ConfigEncodePublicKey() string {
	raw, _ := node.ipfsNode.PrivateKey.GetPublic().Raw()
	return crypto.ConfigEncodeKey(raw)
}

// CoreAPI returns the Core API of the IPFS node
func (node *KuboNode) CoreAPI() icore.CoreAPI {
	return node.coreAPI
}

// Close closes the connections of IPFS node
func (node *KuboNode) Close() error {
	return node.ipfsNode.Close()
}

func setupPlugins(externalPluginsPath string) error {
	log.Debug("loading IPFS plugins")

	// Load any external plugins if available on externalPluginsPath
	plugins, err := loader.NewPluginLoader(filepath.Join(externalPluginsPath, "plugins"))
	if err != nil {
		return fmt.Errorf("error loading plugins: %s", err)
	}

	// Load preloaded and external plugins
	if err := plugins.Initialize(); err != nil {
		return fmt.Errorf("error initializing plugins: %s", err)
	}

	if err := plugins.Inject(); err != nil {
		return fmt.Errorf("error initializing plugins: %s", err)
	}

	log.Debug("finished loading IPFS plugins")

	return nil
}

func doInit(repoRoot string) error {
	log.Debugf("initializing IPFS node at %s", repoRoot)

	if err := util.CheckWritable(repoRoot); err != nil {
		return err
	}

	if fsrepo.IsInitialized(repoRoot) {
		log.Debug("ipfs configuration file already exists!")
		return nil
	}

	identity, err := config.CreateIdentity(os.Stdout, []options.KeyGenerateOption{
		options.Key.Type(algorithm),
	})
	if err != nil {
		return err
	}

	var conf *config.Config
	conf, err = config.InitWithIdentity(identity)
	if err != nil {
		return err
	}

	return fsrepo.Init(repoRoot, conf)
}

func createNode(ctx context.Context, repoPath string) (*core.IpfsNode, error) {
	log.Debug("creating IPFS node")

	// Open the repo
	repo, err := fsrepo.Open(repoPath)
	if err != nil {
		return nil, err
	}

	// Construct the node
	nodeOptions := &core.BuildCfg{
		Online: true,
		// Routing: libp2p.DHTOption, // This option sets the node to be a full DHT node (both fetching and storing DHT Records)
		// Routing: libp2p.DHTClientOption, // This option sets the node to be a client DHT node (only fetching records)
		Routing: libp2p.DHTClientOption,
		Repo:    repo,
		ExtraOpts: map[string]bool{
			"pubsub": true,
		},
	}

	nd, err := core.NewNode(ctx, nodeOptions)
	if err != nil {
		return nil, err
	}
	emptyDir := unixfs.EmptyDirNode()

	// pin recursively because this might already be pinned
	// and doing a direct pin would throw an error in that case
	err = nd.Pinning.Pin(ctx, emptyDir, true, "")
	if err != nil {
		return nil, err
	}

	err = nd.Pinning.Flush(ctx)
	if err != nil {
		return nil, err
	}

	err = nd.Namesys.Publish(ctx, nd.PrivateKey, path.FromCid(emptyDir.Cid()))
	if err != nil {
		return nil, err
	}

	log.Debug("finished creating IPFS node")

	return nd, nil
}
