package ipfs

import (
	logging "github.com/ipfs/go-log/v2"
	icore "github.com/ipfs/kubo/core/coreiface"
	"github.com/ipfs/kubo/core/coreiface/options"
	"github.com/libp2p/go-libp2p/core/peer"
)

var (
	loggingSetLogLevel = logging.SetLogLevel
	log                = logging.Logger("specters:ipfs")
)

const (
	algorithm = options.Ed25519Key
)

// Node is an interface for IPFS node
type Node interface {
	// Connect connects the Node to the IPFS peers
	Connect(peerInfos []peer.AddrInfo) error

	// ID returns the node peer ID
	ID() string

	// ConfigEncodePrivateKey returns the node private key encoded
	ConfigEncodePrivateKey() string

	// ConfigEncodePublicKey returns the node public key encoded
	ConfigEncodePublicKey() string

	// CoreAPI returns the Core API of the IPFS node
	CoreAPI() icore.CoreAPI

	// Close closes the connections of IPFS node
	Close() error
}
