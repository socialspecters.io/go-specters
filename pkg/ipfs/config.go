package ipfs

// Config specifies service settings.
type Config struct {
	Debug    bool
	RepoPath string
}
