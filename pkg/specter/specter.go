package specter

import (
	logging "github.com/ipfs/go-log/v2"
	"gitlab.com/socialspecters.io/go-specters/pkg/db"
	"gitlab.com/socialspecters.io/go-specters/pkg/ipfs"
	"gitlab.com/socialspecters.io/go-specters/pkg/util"
)

var (
	loggingSetLogLevel = logging.SetLogLevel
	log                = logging.Logger("specters:specter")
)

const profileVersion = "1"

// Specter is an instance of a user
type Specter struct {
	publicProfile db.PublicProfile
}

// NewSpecter creates a new Specter
func NewSpecter(ipfsNode ipfs.Node, db db.DB, config Config) (*Specter, error) {
	var err error

	err = loggingSetLogLevel("specters:specter", util.LevelFromDebugFlag(config.Debug))
	if err != nil {
		return nil, err
	}

	log.Info("creating Specter")
	if err != nil {
		return nil, err
	}

	publicProfile, err := db.PublicProfile()
	if err != nil {
		return nil, err
	}

	err = createProfile(publicProfile, ipfsNode)
	if err != nil {
		return nil, err
	}

	log.Info("finished creating specter")

	return &Specter{
		publicProfile: publicProfile,
	}, nil
}

// PublicProfileAddress get the public profile address of the Specter
func (specter *Specter) PublicProfileAddress() string {
	return specter.publicProfile.Address()
}

// Close closes the connections of Specter
func (specter *Specter) Close() error {
	return specter.publicProfile.Close()
}

func createProfile(publicProfile db.PublicProfile, ipfsNode ipfs.Node) error {
	version, err := publicProfile.Get("version")
	if err != nil {
		return err
	}

	if string(version) != profileVersion {
		log.Infof("creating specter profile version %s", profileVersion)

		err = publicProfile.Put("version", []byte(profileVersion))
		if err != nil {
			return err
		}
		err = publicProfile.Put("peerID", []byte(ipfsNode.ID()))
		if err != nil {
			return err
		}
		err = publicProfile.Put("publicKey", []byte(ipfsNode.ConfigEncodePublicKey()))
		if err != nil {
			return err
		}

		log.Info("finished creating specter profile")
	}

	return nil
}
