package util

import (
	"fmt"
	"os"
	"path/filepath"
)

// LevelFromDebugFlag returns the debug or info log level.
func LevelFromDebugFlag(debug bool) string {
	if debug {
		return "debug"
	}
	return "info"
}

// CheckWritable checks if a directory exists and create it
func CheckWritable(dir string) error {
	_, err := os.Stat(dir)
	if err == nil {
		// dir exists, make sure we can write to it
		testFile := filepath.Join(dir, "test")
		fi, err := os.Create(testFile)
		if err != nil {
			if os.IsPermission(err) {
				return fmt.Errorf("%s is not writeable by the current user", dir)
			}
			return fmt.Errorf("unexpected error while checking writeablility of repo root: %s", err)
		}
		fi.Close()
		return os.Remove(testFile)
	}

	if os.IsNotExist(err) {
		// dir doesn't exist, check that we can create it
		return os.Mkdir(dir, 0o750)
	}

	if os.IsPermission(err) {
		return fmt.Errorf("cannot write to %s, incorrect permissions", err)
	}

	return err
}
