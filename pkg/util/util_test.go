package util

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLevelFromDebugFlag(t *testing.T) {
	tests := []struct {
		name      string
		debug     bool
		expecting string
	}{
		{
			name:      "Info logging level",
			debug:     false,
			expecting: "info",
		},
		{
			name:      "Debug logging level",
			debug:     true,
			expecting: "debug",
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			assert.Equal(tt, tc.expecting, LevelFromDebugFlag(tc.debug))
		})
	}
}
