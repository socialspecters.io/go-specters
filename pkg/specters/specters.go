package specters

import (
	"context"
	"fmt"
	logging "github.com/ipfs/go-log/v2"
	"gitlab.com/socialspecters.io/go-specters/pkg/db"
	"gitlab.com/socialspecters.io/go-specters/pkg/ipfs"
	"gitlab.com/socialspecters.io/go-specters/pkg/specter"
	"gitlab.com/socialspecters.io/go-specters/pkg/util"
)

var (
	loggingSetLogLevel = logging.SetLogLevel
	log                = logging.Logger("specters:specters")
)

// Specters is a specters manager
type Specters struct {
	IPFSNode ipfs.Node
	DB       db.DB
	Specter  *specter.Specter
}

// Init initializes IPFS, OrbitDB and own Specter user
func Init(ctx context.Context, config Config) (*Specters, error) {
	var err error
	specters := &Specters{}

	err = loggingSetLogLevel("specters:specters", util.LevelFromDebugFlag(config.Debug))
	if err != nil {
		return nil, err
	}

	log.Info("init Specters")

	err = util.CheckWritable(config.Repo)
	if err != nil {
		return nil, err
	}

	ipfsConfig := ipfs.Config{
		Debug:    config.Debug,
		RepoPath: fmt.Sprintf("%s/ipfs", config.Repo),
	}

	boostrapPeers, err := DefaultBoostrapPeers()
	if err != nil {
		log.Fatal(err)
	}

	specters.IPFSNode, err = ipfs.NewKuboNode(ctx, ipfsConfig)
	if err != nil {
		log.Fatal(err)
	}

	specters.DB, err = db.NewOrbitDB(ctx, specters.IPFSNode, db.Config{
		Debug:     config.Debug,
		CachePath: fmt.Sprintf("%s/orbitdb", config.Repo),
	})
	if err != nil {
		log.Fatal(err)
	}

	specters.Specter, err = specter.NewSpecter(specters.IPFSNode, specters.DB, specter.Config{
		Debug: config.Debug,
	})
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		err := specters.IPFSNode.Connect(boostrapPeers)
		if err != nil {
			log.Infof("failed connect to peers: %s", err)
		}
	}()

	log.Info("finished init Specters")

	return specters, nil
}

// Close closes all connections of the Specters manager
func (s *Specters) Close() {
	if err := s.Specter.Close(); err != nil {
		log.Fatal(err)
	}
	if err := s.DB.Close(); err != nil {
		log.Fatal(err)
	}
	if err := s.IPFSNode.Close(); err != nil {
		log.Fatal(err)
	}
}
