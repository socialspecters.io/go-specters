package specters

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDefaultBoostrapPeers(t *testing.T) {
	tests := []struct {
		name           string
		bootstrapPeers []string
		expectingErr   bool
	}{
		{
			name:           "All success no error",
			bootstrapPeers: spectersBootstrapPeers,
			expectingErr:   false,
		},
		{
			name:           "Error parsing boostrap peers",
			bootstrapPeers: []string{"/foo/127.0.0.1/tcp/6006"},
			expectingErr:   true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			origBootstrapPeers := spectersBootstrapPeers
			spectersBootstrapPeers = tc.bootstrapPeers
			defer func() { spectersBootstrapPeers = origBootstrapPeers }()

			_, err := DefaultBoostrapPeers()
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist, err)
		})
	}
}
