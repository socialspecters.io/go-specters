package specters

import (
	"github.com/ipfs/kubo/config"
	"github.com/libp2p/go-libp2p/core/peer"
	"github.com/multiformats/go-multiaddr"
)

var (
	spectersBootstrapPeers []string
)

// Config specifies service settings.
type Config struct {
	Debug bool
	Repo  string
}

// DefaultBoostrapPeers returns the default bootstrap peers for Specters
func DefaultBoostrapPeers() (peers []peer.AddrInfo, err error) {
	ipfsPeers, err := config.DefaultBootstrapPeers()
	if err != nil {
		return nil, err
	}
	peers, err = parseBootstrapPeers(spectersBootstrapPeers)
	if err != nil {
		return nil, err
	}
	return append(peers, ipfsPeers...), nil
}

func parseBootstrapPeers(addrs []string) ([]peer.AddrInfo, error) {
	maddrs := make([]multiaddr.Multiaddr, len(addrs))
	for i, addr := range addrs {
		var err error
		maddrs[i], err = multiaddr.NewMultiaddr(addr)
		if err != nil {
			return nil, err
		}
	}
	return peer.AddrInfosFromP2pAddrs(maddrs...)
}
